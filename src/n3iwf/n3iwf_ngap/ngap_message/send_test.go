package ngap_message_test

import (
	"free5gc/lib/aper"
	"free5gc/lib/ngap/ngapType"
	"free5gc/src/n3iwf/factory"
	"free5gc/src/n3iwf/n3iwf_ngap/n3iwf_sctp"
	"free5gc/src/n3iwf/n3iwf_ngap/ngap_message"
	"gopkg.in/yaml.v2"
	"sync"
	"testing"
)

var config = `
AMFAddress:
  - IP: 127.0.0.1
`

func TestSendErrorIndication(t *testing.T) {
	var testConfig factory.Configuration
	err := yaml.Unmarshal([]byte(config), &testConfig)
	if err != nil {
		t.Fatalf("Unmarshal failed. %+v", err)
	}

	wg := sync.WaitGroup{}
	n3iwf_sctp.SetupSCTPConnection(testConfig.AMFAddress, &wg)

	cause := buildCause(ngapType.CausePresentProtocol, ngapType.CauseProtocolPresentAbstractSyntaxErrorReject)

	procedureCode := ngapType.ProcedureCodeNGSetup
	triggeringMessage := ngapType.TriggeringMessagePresentSuccessfulOutcome
	procedureCriticality := ngapType.CriticalityPresentReject
	var iesCriticalityDiagnostics ngapType.CriticalityDiagnosticsIEList
	item := buildCriticalityDiagnosticsIEItem(ngapType.CriticalityPresentReject, 13, ngapType.TypeOfErrorPresentMissing)
	iesCriticalityDiagnostics.List = append(iesCriticalityDiagnostics.List, item)
	criticalityDiagnostics := buildCriticalityDiagnostics(&procedureCode, &triggeringMessage, &procedureCriticality, &iesCriticalityDiagnostics)

	ngap_message.SendErrorIndication("127.0.0.1:38412", nil, nil, cause, &criticalityDiagnostics)

	wg.Wait()
}

func buildCriticalityDiagnostics(
	procedureCode *int64,
	triggeringMessage *aper.Enumerated,
	procedureCriticality *aper.Enumerated,
	iesCriticalityDiagnostics *ngapType.CriticalityDiagnosticsIEList) (criticalityDiagnostics ngapType.CriticalityDiagnostics) {

	if procedureCode != nil {
		criticalityDiagnostics.ProcedureCode = new(ngapType.ProcedureCode)
		criticalityDiagnostics.ProcedureCode.Value = *procedureCode
	}

	if triggeringMessage != nil {
		criticalityDiagnostics.TriggeringMessage = new(ngapType.TriggeringMessage)
		criticalityDiagnostics.TriggeringMessage.Value = *triggeringMessage
	}

	if procedureCriticality != nil {
		criticalityDiagnostics.ProcedureCriticality = new(ngapType.Criticality)
		criticalityDiagnostics.ProcedureCriticality.Value = *procedureCriticality
	}

	if iesCriticalityDiagnostics != nil {
		criticalityDiagnostics.IEsCriticalityDiagnostics = iesCriticalityDiagnostics
	}

	return criticalityDiagnostics
}

func buildCriticalityDiagnosticsIEItem(ieCriticality aper.Enumerated, ieID int64, typeOfErr aper.Enumerated) (item ngapType.CriticalityDiagnosticsIEItem) {

	item = ngapType.CriticalityDiagnosticsIEItem{
		IECriticality: ngapType.Criticality{
			Value: ieCriticality,
		},
		IEID: ngapType.ProtocolIEID{
			Value: ieID,
		},
		TypeOfError: ngapType.TypeOfError{
			Value: typeOfErr,
		},
	}

	return item
}

func buildCause(present int, value aper.Enumerated) (cause *ngapType.Cause) {
	cause = new(ngapType.Cause)
	cause.Present = present

	switch present {
	case ngapType.CausePresentRadioNetwork:
		cause.RadioNetwork = new(ngapType.CauseRadioNetwork)
		cause.RadioNetwork.Value = value
	case ngapType.CausePresentTransport:
		cause.Transport = new(ngapType.CauseTransport)
		cause.Transport.Value = value
	case ngapType.CausePresentNas:
		cause.Nas = new(ngapType.CauseNas)
		cause.Nas.Value = value
	case ngapType.CausePresentProtocol:
		cause.Protocol = new(ngapType.CauseProtocol)
		cause.Protocol.Value = value
	case ngapType.CausePresentMisc:
		cause.Misc = new(ngapType.CauseMisc)
		cause.Misc.Value = value
	case ngapType.CausePresentNothing:
	}

	return
}
