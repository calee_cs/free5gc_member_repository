package n3iwf_context

import (
	"github.com/sirupsen/logrus"

	"free5gc/src/n3iwf/logger"
)

var contextLog *logrus.Entry

var n3iwfContext = N3IWFContext{}
var ranUeNgapIdGenerator int64 = 0

type N3IWFContext struct {
	NFInfo  N3IWFNFInfo
	UePool  map[int64]*N3IWFUe   // RanUeNgapID as key
	AMFPool map[string]*N3IWFAMF // SCTPSessionID as key
}

func init() {
	// init log
	contextLog = logger.ContextLog

	// init context
	N3IWFSelf().UePool = make(map[int64]*N3IWFUe)
	N3IWFSelf().AMFPool = make(map[string]*N3IWFAMF)
}

// Create new N3IWF context
func N3IWFSelf() *N3IWFContext {
	return &n3iwfContext
}

func (context *N3IWFContext) NewN3iwfUe() *N3IWFUe {
	n3iwfUe := &N3IWFUe{}
	n3iwfUe.init()

	ranUeNgapIdGenerator %= MaxValueOfRanUeNgapID
	ranUeNgapIdGenerator++
	for {
		if _, double := context.UePool[ranUeNgapIdGenerator]; double {
			ranUeNgapIdGenerator++
		} else {
			break
		}
	}

	n3iwfUe.RanUeNgapId = ranUeNgapIdGenerator
	n3iwfUe.AmfUeNgapId = AmfUeNgapIdUnspecified
	context.UePool[n3iwfUe.RanUeNgapId] = n3iwfUe
	return n3iwfUe
}

func (context *N3IWFContext) NewN3iwfAmf(sessionID string) *N3IWFAMF {
	if amf, ok := context.AMFPool[sessionID]; ok {
		contextLog.Warn("[Context] NewN3iwfAmf(): AMF entry already exists.")
		return amf
	} else {
		amf = &N3IWFAMF{}
		context.AMFPool[sessionID] = amf
		return amf
	}
}

func (context *N3IWFContext) FindAMFBySCTPSessionID(sctpSessionID string) *N3IWFAMF {
	amf, ok := context.AMFPool[sctpSessionID]
	if !ok {
		contextLog.Warnf("[Context] FindAMF(): AMF not found. SessionID: %s", sctpSessionID)
	}
	return amf
}

func (context *N3IWFContext) FindUeByAmfUeNgapID(amfUeNgapID int64) *N3IWFUe {
	for _, ue := range context.UePool {
		if ue.AmfUeNgapId == amfUeNgapID {
			return ue
		}
	}

	return nil
}

func (context *N3IWFContext) FindUeByRanUeNgapID(ranUeNgapID int64) *N3IWFUe {
	if n3iwfUE, ok := context.UePool[ranUeNgapID]; ok {
		return n3iwfUE
	} else {
		return nil
	}
}
