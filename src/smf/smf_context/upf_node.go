package smf_context

import (
	"fmt"
	"free5gc/lib/pfcp/pfcpType"
	"net"
	"reflect"
)

var upfPool map[string]*UPF

func init() {
	upfPool = make(map[string]*UPF)
}

type UPTunnel struct {
	Node  *UPF
	ULPDR *PDR
	DLPDR *PDR

	ULTEID uint32
	DLTEID uint32
}

type UPF struct {
	NodeID   pfcpType.NodeID
	UPIPInfo pfcpType.UserPlaneIPResourceInformation

	pdrPool         map[uint16]*PDR
	farPool         map[uint32]*FAR
	barPool         map[uint8]*BAR
	urrPool         map[uint32]*URR
	qerPool         map[uint32]*QER
	pdrCount        uint16
	farCount        uint32
	barCount        uint8
	urrCount        uint32
	qerCount        uint32
	TEIDCount       uint32
	pdrIdReuseQueue []uint16
	farIdReuseQueue []uint32
	barIdReuseQueue []uint8
}

func AddUPF(nodeId *pfcpType.NodeID) (upf *UPF) {
	upf = new(UPF)
	key, _ := generateUpfIdFromNodeId(nodeId)
	upfPool[key] = upf
	upf.NodeID = *nodeId
	upf.pdrPool = make(map[uint16]*PDR)
	upf.farPool = make(map[uint32]*FAR)
	upf.barPool = make(map[uint8]*BAR)
	upf.qerPool = make(map[uint32]*QER)
	upf.urrPool = make(map[uint32]*URR)
	upf.pdrIdReuseQueue = make([]uint16, 0)
	upf.farIdReuseQueue = make([]uint32, 0)
	upf.barIdReuseQueue = make([]uint8, 0)

	return
}

func generateUpfIdFromNodeId(nodeId *pfcpType.NodeID) (string, error) {
	switch nodeId.NodeIdType {
	case pfcpType.NodeIdTypeIpv4Address, pfcpType.NodeIdTypeIpv6Address:
		return net.IP(nodeId.NodeIdValue).String(), nil
	case pfcpType.NodeIdTypeFqdn:
		return string(nodeId.NodeIdValue), nil
	default:
		return "", fmt.Errorf("Invalid Node ID type: %v", nodeId.NodeIdType)
	}
}

func (upf *UPF) GenerateTEID() uint32 {
	upf.TEIDCount++
	return upf.TEIDCount
}

func RetrieveUPFNodeByNodeId(nodeId pfcpType.NodeID) (upf *UPF) {
	for _, upf := range upfPool {
		if reflect.DeepEqual(upf.NodeID, nodeId) {
			return upf
		}
	}
	return nil
}

func RemoveUPFNodeByNodeId(nodeId pfcpType.NodeID) {
	for upfID, upf := range upfPool {
		if reflect.DeepEqual(upf.NodeID, nodeId) {
			delete(upfPool, upfID)
			break
		}
	}
}

func SelectUPFByDnn(Dnn string) *UPF {
	for _, upf := range upfPool {
		if !upf.UPIPInfo.Assoni || string(upf.UPIPInfo.NetworkInstance) == Dnn {
			return upf
		}
	}
	return nil
}

func (upf *UPF) pdrID() uint16 {

	if len(upf.pdrIdReuseQueue) == 0 {
		upf.pdrCount++
		return upf.pdrCount
	} else {
		pdrID := upf.pdrIdReuseQueue[0]
		upf.pdrIdReuseQueue = upf.pdrIdReuseQueue[1:]
		return pdrID
	}

}

func (upf *UPF) farID() uint32 {

	if len(upf.farIdReuseQueue) == 0 {
		upf.farCount++
		return upf.farCount
	} else {
		farID := upf.farIdReuseQueue[0]
		upf.farIdReuseQueue = upf.farIdReuseQueue[1:]
		return farID
	}
}

func (upf *UPF) barID() uint8 {

	if len(upf.barIdReuseQueue) == 0 {
		upf.barCount++
		return upf.barCount
	} else {
		barID := upf.barIdReuseQueue[0]
		upf.barIdReuseQueue = upf.barIdReuseQueue[1:]
		return barID
	}
}

func (upf *UPF) AddPDR() (pdr *PDR) {
	pdr = new(PDR)
	pdr.PDRID = uint16(upf.pdrID())
	upf.pdrPool[pdr.PDRID] = pdr
	pdr.FAR = upf.AddFAR()
	return pdr
}

func (upf *UPF) AddFAR() (far *FAR) {
	far = new(FAR)
	far.FARID = upf.farID()
	upf.farPool[far.FARID] = far
	return far
}

func (upf *UPF) AddBAR() (bar *BAR) {
	bar = new(BAR)
	bar.BARID = uint8(upf.barID())
	upf.barPool[bar.BARID] = bar
	return bar
}

func (upf *UPF) RemovePDR(pdr *PDR) {

	upf.RemovePDRID(pdr.PDRID)
	delete(upf.pdrPool, pdr.PDRID)
}

func (upf *UPF) RemoveFAR(far *FAR) {

	upf.RemoveFARID(far.FARID)
	delete(upf.farPool, far.FARID)
}

func (upf *UPF) RemoveBAR(bar *BAR) {

	upf.RemoveBARID(bar.BARID)
	delete(upf.barPool, bar.BARID)
}

func (upf *UPF) RemovePDRID(PDRID uint16) {
	upf.pdrIdReuseQueue = append(upf.pdrIdReuseQueue, PDRID)
}

func (upf *UPF) RemoveFARID(FARID uint32) {
	upf.farIdReuseQueue = append(upf.farIdReuseQueue, FARID)
}

func (upf *UPF) RemoveBARID(BARID uint8) {
	upf.barIdReuseQueue = append(upf.barIdReuseQueue, BARID)
}

func (upf *UPF) PrintPDRPoolStatus() {
	for k := range upf.pdrPool {
		fmt.Println("PDR ID: ", k, " using")
	}
}

func (upf *UPF) PrintFARPoolStatus() {
	for k := range upf.farPool {
		fmt.Println("FAR ID: ", k, " using")
	}
}

func (upf *UPF) PrintBARPoolStatus() {
	for k := range upf.barPool {
		fmt.Println("BAR ID: ", k, " using")
	}
}
